json.array!(@logins) do |login|
  json.extract! login, :id, :title, : body
  json.url login_url(login, format: :json)
end
